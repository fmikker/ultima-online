; Mining script by Pukas
;; 20070127

;; OTHER VARIABLES
set %v_reset_time 30
set %v_next_pos_cnt 1

;; USE HARDCODED POSITIONS
set %g_use_hardcoded_pos #false

;set %g_pos_x1 0
;set %g_pos_y1 0
;set %g_pos_x2 0
;set %g_pos_y2 0
;set %g_pos_x3 0
;set %g_pos_y3 0

;; DO NOT MODIFY BELOW THIS COMMENT UNLESS YOU KNOW WHAT YOU ARE DOING

set %v_radius 2

;; GLOBAL VARS

set %g_rel_pos_x 0
set %g_rel_pos_y 0

set %g_finished_msgs_var g_finished_msg
set %g_finished_msgs_cnt 0

set %g_max_x %v_radius
set %g_min_x %v_radius * -1
set %g_max_y %v_radius
set %g_min_y %v_radius * -1

;; STATIC VALUES

set %s_finished_msgs you_stop_mining|that_is_too_far_away|you_can't_mine_or_dig_anything_there|you_can't_use_this_on
set %s_success_msgs you put
set %s_ok_tile cave_floor

set %s_digtypes |220|221|222|223|224|225|226|227|228|229|230|231|236|237|238|239|240|241|242|243|
    +244|245|246|247|252|253|254|255|256|257|258|259|260|261|262|263|268|269|270|271|
    +272|273|274|275|276|277|278|279|286|287|288|289|290|291|292|293|294|296|296|297|
    +321|322|323|324|467|468|469|470|471|472|473|474|476|477|478|479|480|481|482|483|
    +484|485|486|487|492|493|494|495|543|544|545|546|547|548|549|550|551|552|553|554|
    +555|556|557|558|559|560|561|562|563|564|565|566|567|568|569|570|571|572|573|574|
    +575|576|577|578|579|581|582|583|584|585|586|587|588|589|590|591|592|593|594|595|
    +596|597|598|599|600|601|610|611|612|613|1010|1741|1742|1743|1744|1745|1746|1747|
    +1748|1749|1750|1751|1752|1753|1754|1755|1756|1757|1771|1772|1773|1774|1775|1776|
    +1777|1778|1779|1780|1781|1782|1783|1784|1785|1786|1787|1788|1789|1790|1801|1802|
    +1803|1804|1805|1806|1807|1808|1809|1811|1812|1813|1814|1815|1816|1817|1818|1819|
    +1820|1821|1822|1823|1824|1831|1832|1833|1834|1835|1836|1837|1838|1839|1840|1841|
    +1842|1843|1844|1845|1846|1847|1848|1849|1850|1851|1852|1853|1854|1861|1862|1863|
    +1864|1865|1866|1867|1868|1869|1870|1871|1872|1873|1874|1875|1876|1877|1878|1879|
    +1880|1881|1882|1883|1884|1981|1982|1983|1984|1985|1986|1987|1988|1989|1990|1991|
    +1992|1993|1994|1995|1996|1997|1998|1999|2000|2001|2002|2003|2004|2028|2029|2030|
    +2031|2032|2033|2100|2101|2102|2103|2104|2105|1339|1340|1341|1342|1343|1344|1345|
    +1346|1347|1348|1349|1350|1351|1352|1353|1354|1355|1356|1357|1358|1359|

set %s_sandtypes |22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|
    +48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|68|69|70|71|72|73|74|75|286|287|288|289|
    +290|291|292|293|294|295|296|297|298|299|300|301|402|424|425|426|427|441|442|443|444|
    +445|446|447|448|449|450|451|452|453|454|455|456|457|458|459|460|461|462|463|464|465|
    +642|643|644|645|650|651|652|653|654|655|656|657|821|822|823|824|825|826|827|828|833|
    +834|835|836|845|846|847|848|849|850|851|852|857|858|859|860|951|952|953|954|955|956|
    +957|958|967|968|969|970|1447|1448|1449|1450|1451|1452|1453|1454|1455|1456|1457|1458|
    +1611|1612|1613|1614|1615|1616|1617|1618|1623|1624|1625|1626|1635|1636|1637|1638|1639|
    +1640|1641|1642|1647|1648|1649|1650|

set %s_default_lpc 100

;; DEBUG FLAG
set %DEBUG #false

;; SCRIPT

tile init
initEvents

gosub string exp %s_finished_msgs | %g_finished_msgs_var
set %g_finished_msgs_cnt #result

wait 2s

if ! %g_use_hardcoded_pos
{
 gosub assignLocations
 menu hide
}

set %g_last_charposx #charposx
set %g_last_charposy #charposy
set %g_loop_cnt %v_next_pos_cnt
set %g_curr_pos 3

mainLoop:

event sysmessage Starting new mining loop

set %g_loop_cnt %g_loop_cnt + 1

if %g_loop_cnt >= %v_next_pos_cnt
{
      set %g_curr_pos %g_curr_pos + 1
      if %g_curr_pos > 3
      {
         set %g_curr_pos 1
      }

      event sysmessage Moving to location # , %g_curr_pos

      event pathfind  %g_pos_x . %g_curr_pos %g_pos_y . %g_curr_pos

      wait 2s
      event sysmessage Starting in 8sec...
      wait 4s
      event sysmessage Starting in 4sec...
      wait 2s
      event sysmessage Starting in 2sec...
      wait 1s
      event sysmessage Starting in 1sec...
      wait 1s
}

for %g_rel_pos_x  %g_min_x %g_max_x
{
     for %g_rel_pos_y  %g_min_y %g_max_y
     {

         if #charposx <> %g_last_charposx || #charposy <> %g_last_charposy
         {
            event sysmessage You moved, rescanning area!
            gosub scanArea

            if ! #result
            {
               event sysmessage Aborting script...
               halt
            }

            set %g_last_charposx #charposx
            set %g_last_charposy #charposy
         }
         set %ml_pos_x #charposx + %g_rel_pos_x
         set %ml_pos_y #charposy + %g_rel_pos_y

         gosub checkIfMine %ml_pos_x %ml_pos_y

         if ! #result
         {
            continue
         }

         event sysmessage Mining at ( , %ml_pos_x , , , %ml_pos_y , )

         gosub findTool

         if #result = -1
         {
            display ok You dont have anything to mine with!
            halt
         }

         set #lobjectid #result

         event macro 17
         target 5s
         event macro 22

         wait 2s

         for %sleep_loops 1 %v_reset_time
         {
             gosub checkIfDone

             wait 1s

             if #result
             {
                break
             }
         }
     }
}

goto mainLoop


;; Check how many tiles of water there are
sub scanArea

set %sa_sand_count 0
set %sa_mine_count 0
set #lpc 1000

for %sa_x %g_min_x %g_max_x
{
     for %sa_y %g_min_y %g_max_y
     {
         set %sa_pos_x #charposx + %sa_x
         set %sa_pos_y #charposy + %sa_y

         gosub checkIfMine %sa_pos_x %sa_pos_y

         if #result = 1
         {
            set %sa_sand_count %sa_sand_count + 1
         }
         if #result = 2
         {
            set %sa_mine_count %sa_mine_count + 1
         }
     }
}

set #lpc %s_default_lpc

if %sa_sand_count = 0 && %sa_mine_count = 0
{
   event sysmessage No positions available for mining!
   return #false
}

event sysmessage Found %sa_mine_count (mine) and %sa_sand_count (sand) tiles to mine from

return #true

sub findTool
finditem WWF_TWF_QPF_NPF_WTH c
if #findkind = -1
{
   return -1
}

return #findid

;; Check if fishin loop is done
sub checkIfDone

if %g_finished_msgs_cnt = 0
{
   display No finished msgs found% Aborting!
   halt
}

for %f_m_c 1 %g_finished_msgs_cnt
{
    if %g_finished_msg . %f_m_c in #sysmsg
    {
       if %DEBUG
       {
          event sysmessage Stopped mining! Found: %g_finished_msg . %f_m_c
       }
       return #true
    }

}
return #false




sub checkIfMine

if %0 <> 2
{
   return #false
}


tile cnt %1 %2 #curskind

tile get %1 %2 1

if sand in #tilename
{
   if , #tiletype , | in %s_sandtypes
   {
      set #ltargetx %1
      set #ltargety %2
      set #ltargetz #tilez
      set #ltargettile #tiletype
      set #ltargetkind 2
      return 1
   }
}

if | , #tiletype , | in %s_digtypes
{
       set #ltargetx %1
       set #ltargety %2
       set #ltargetz #tilez
       set #ltargettile #tiletype
       set #ltargetkind 3
       return 2
}
if #tilename = rock && #tilecnt > 1
{
   for %tilecnt 2 #tilecnt
   {
       tile get %1 %2 %tilecnt

       if cave in #tilename
       {
       set #ltargetx %1
       set #ltargety %2
       set #ltargetz #tilez
       set #ltargettile #tiletype
       set #ltargetkind 3
       return 2
       }
    }
}
if #tilename = dirt && #tilecnt > 1

   tile get %1 %2 #tilecnt

   if cave_floor in #tilename
   {
       set #ltargetx %1
       set #ltargety %2
       set #ltargetz #tilez
       set #ltargettile #tiletype
       set #ltargetkind 3
       return 2
   }
}

;event sysmessage NOT OK: #tilename %1 %2
return #false

sub checkNextPos

;event sysmessage cNPos %1 %2 %3 -> #result

set %cnp_centerpos_x %1
set %cnp_centerpos_y %2
set %cnp_centerpos_offset %3

if %0 <> 3
{
   return -1
}

gosub checkFrame %cnp_centerpos_x %cnp_centerpos_y %cnp_centerpos_offset
if #result = -1
{
   set %cnp_one_less %cnp_centerpos_offset - 1
   gosub checkFrame %cnp_centerpos_x %cnp_centerpos_y %cnp_one_less
}

return #result

sub checkFrame

;event sysmessage checkFrame %1 %2 %3

if %0 <> 3
{
   return -1
}

set %cf_centerpos_x %1
set %cf_centerpos_y %2
set %cf_neg_offset %3 * -1
set %cf_pos_offset %3

for %cf_curr_offset %cf_neg_offset %cf_pos_offset
{
    set %cf_curr_x %cf_centerpos_x + %cf_curr_offset
    set %cf_curr_y %cf_centerpos_y + %cf_curr_offset

    gosub checkIfMine %cf_curr_x %cf_curr_y

    ;event sysmessage checkFrame cf_curr_x %cf_curr_x cf_curr_y %cf_curr_y #result

    if #result
    {
       return %cf_curr_x , | , %cf_curr_y , | , #ltargetz
    }

    set %cf_curr_x %cf_centerpos_x + %cf_curr_offset + 1
    set %cf_curr_y %cf_centerpos_y + %cf_curr_offset



    gosub checkIfMine %cf_curr_x %cf_curr_y

    ;event sysmessage checkFrame cf_curr_x %cf_curr_x cf_curr_y %cf_curr_y #result
    if #result
    {
       return %cf_curr_x , | , %cf_curr_y , | , #ltargetz
    }

    set %cf_curr_x %cf_centerpos_x + %cf_curr_offset
    set %cf_curr_y %cf_centerpos_y + %cf_curr_offset + 1

    gosub checkIfMine %cf_curr_x %cf_curr_y

    ;event sysmessage checkFrame cf_curr_x %cf_curr_x cf_curr_y %cf_curr_y #result
    if #result
    {
       return %cf_curr_x , | , %cf_curr_y , | , #ltargetz
    }
}
return -1

sub assignLocations

event sysmessage Set first position

gosub fetchpos 1st

set %g_pos_x1 #charposx
set %g_pos_y1 #charposy

gosub scanArea %g_pos_x1 %g_pos_y1
gosub checkScanResult

wait 1s

event sysmessage Set second position

gosub fetchpos 2nd

set %g_pos_x2 #charposx
set %g_pos_y2 #charposy


gosub scanArea %g_pos_x2 %g_pos_y2
gosub checkScanResult

wait 1s

event sysmessage Set third position

gosub fetchpos 3rd

set %g_pos_x3 #charposx
set %g_pos_y3 #charposy

gosub scanArea %g_pos_x3 %g_pos_y3
gosub checkScanResult

wait 1s

return #true

sub waitForCurs
if #targcurs <> %1
{
   sleep 25
   gosub waitForCurs
}
return

sub checkScanResult

if ! #result
{
   event sysmessage Cannot mine from that target!
   event sysmessage Aborting...
   halt
}

return #true


sub fetchpos

menu clear
menu show
menu window size 135 50
menu button button1 43 25 50 20 OK
menu text label 5 5 Stand on %1 position

set #menubutton n/a
loop:
if #menubutton = button1
{
 ;menu hide
 set #menubutton n/a
 return #charposx , | , #charposy
}
goto loop



;==================================
; Script Name: Gravisoft's String Implode/Explode SUB
; Author: Gravix
; Version: 1.1
; Client Tested with: 4.0.6a
; EUO version tested with: 1.42.009F
; Shard OSI / FS: Both
; Revision Date: 1/19/04
; Public Release: 1/01/04
; Global Variables Used: None
; Purpose: Implode: Take an array and link into a string separated by a value/char
; Explode: Take a string and break into array based on dividers
;==================================

;SUB string exp %str %div %arry
SUB String
if %5 <> #true
    {
    namespace push
    namespace local Implode-Explode
    }
IF %1 = exp
     {
      SET !c 0
      SET !s %2
    break:
        IF %3 in !s
            {
             SET !c !c + 1
              STR POS !s %3
              SET !p0 #STRRES
              SET !p1 !p0 - 1
              STR LEFT !s !p1
              if %5 <> #true
            SET % . %4 , !c #STRRES
        if %5
            SET ! . %4 , !c #STRRES
              STR DEL !s 1 !p0
              SET !s #STRRES
              GOTO break
            }
        ELSE
            {
            SET !c !c + 1
        if %5
                  SET ! . %4 , !c !s
              Set #result !c
        if %5 <> #true
                  {
                  SET % . %4 , !c !s
            Namespace clear
          Namespace pop
            }
        RETURN #result
            }
      }
RETURN #FALSE
