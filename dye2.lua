    ;==========================================
    ; Script Name: Uncle Dave's Hue Matcher 1.1
    ; Author: Uncle Dave
    ; Version: 1.1
    ; Client Tested with: 4.0.8a
    ; EUO version tested with: 1.42.00A0
    ; Shard OSI / FS: OSI
    ; Revision Date: 2/28/2005
    ; Public Release: 2/28/2005
    ; Global Variables Used: N/A
    ; Purpose: Matches hues with a normal dye
    ; tub. Will match exact colors by selecting
    ; an object, or will find closest indexed
    ; color to match an average screen color.
    ;==========================================
    ;
    ; There are 1000 colors on a normal dye, the
    ; possible range is 2 - 1001. Color 0 is normal
    ; hue (not dyed) and color 1 is black.
    ;
    ; The color grid of a normal dye has 20 columns
    ; and 10 rows starting at offset (34,34) from
    ; the container position. Each color cell is
    ; 8 x 8 pixels.
    ;
    ; There are 5 brightness levels with a default
    ; brighness of 1 [0-4]. The offset to reduce the
    ; brighness is (35,149) and the offset to increase
    ; the brightness is (189,149).
    ;
    ; The OK button is located at offset (235,145).
    ;
    ; The container name is hue_gump, and the
    ; container size is 288_170.
    ;
    ; To find the color of a given row (r) [0-9],
    ; column (c) [0-19] and brightness (b) [0-4],
    ; use the following formula:
    ;
    ; color = 2 + ( 100 * r ) + ( 5 * c ) + ( b )
    ;
    ; To find the row (r) [0-9], column (c) [0-19]
    ; and brightness (b) [0-4] of a given color:
    ;
    ; Subtract 2 from the color
    ; Divide result by 100 to get the row (r)
    ; Divide remainder by 5 to get the column (c)
    ; Remainder is the brightness (b)
    ;
    ; For special dye colors, the container name is
    ; generic_gump, and the container size is 450_450.
    ; The color selection buttons are 25 pixels apart
    ; (from top to top) starting at offset (40,93)
    ; from the container position and the hue selection
    ; buttons are also 25 pixels apart starting at
    ; offset (270,100) from the container position.
    ; The OK button is located at offset (35,410).
    ;
    ; The special dye tub colors are as follows:
    ;
    ; Violet: 1230 - 1235
    ; Tan: 1501 - 1508
    ; Brown: 2012 - 2017
    ; Dark Blue: 1303 - 1308
    ; Forest Green: 1420 - 1426
    ; Pink: 1619 - 1626
    ; Red: 1640 - 1644
    ; Olive: 2001 - 2005
    ;
    ; The leather dye tub buttons are the same as the
    ; special dye tub with an added button to reset the
    ; hue to default. This button is located at offset
    ; (215,410).
    ;
    ; The leather dye tub colors are as follows:
    ;
    ; Dull Copper: 2419 - 2424
    ; Shadow Iron: 2406 - 2412
    ; Copper: 2413 - 2418
    ; Bronze: 2414 - 2418
    ; Golden: 2213 - 2218
    ; Agapite: 2425 - 2430
    ; Verite: 2207 - 2212
    ; Valorite: 2219 - 2224
    ; Reds: 2113 - 2118
    ; Blues: 2119 - 2124
    ; Greens: 2126 - 2130
    ; Yellows: 2213 - 2218
    ;
    gosub ShowMenu
    if #false ; Set to true to rebuild color table
    {
        gosub BuildColorTable
        gosub SaveColorTable
    }
    else
        gosub LoadColorTable
     
    set #menubutton N/A
    menu set message Ready
     
    _main_loop:
    if #menubutton = btnMatchObject
    {
        set #menubutton N/A
        gosub MatchObject
    }
    if #menubutton = btnMatchScreen
    {
        set #menubutton N/A
        gosub MatchScreen
    }
    if #menubutton = btnSetDyeTub
    {
        set #menubutton N/A
        gosub SetDyeTub
    }
    goto _main_loop
     
    sub MatchObject
        gosub Reset
        menu set Message Target the item you wish to match.
        gosub SetLastTarget
        finditem #ltargetid
        if #findkind = -1
        {
            menu set Message Unable to locate object
            return
        }
        menu set CurrentIndex #findcol
        if #findcol < 2 || #findcol > 1001
        {
            menu set Message Color index cannot be matched
            return
        }
        set %matchindex #findcol
        gosub GetIndexedColor %matchindex
        set %color #result
        gosub ColorToRGB %color
        menu set CurrentRed %r
        menu set CurrentGreen %g
        menu set CurrentBlue %b
        menu set CurrentColor %color
        menu set CurrentIndex %matchindex
        menu set MatchRed %r
        menu set MatchGreen %g
        menu set MatchBlue %b
        menu set MatchColor %color
        menu set MatchIndex %matchindex
        menu delete CurrentPreview
        menu Shape CurrentPreview 296 32 17 17 3 7 1 Silver 7 %color
        menu delete MatchPreview
        menu Shape MatchPreview 296 52 17 17 3 7 1 Silver 7 %color
        menu set Message Color index found, click Set Dye Tub to use matched color.
    return
     
    sub MatchScreen
        menu set Message Hover your mouse over the screen coordinates and press Escape.
        _matchscreen_escape:
        onhotkey ESC
            goto _matchscreen_coords
        if #menubutton <> N/A
            return
        goto _matchscreen_escape
        _matchscreen_coords:
        set %x #cursorx
        set %y #cursory
        menu set X %x
        menu set Y %y
        menu set Message Click Match Screen again to match screen coordinates.
        _matchscreen_wait:
        if #menubutton = N/A
            goto _matchscreen_wait
        if #menubutton <> btnMatchScreen
            return
        set #menubutton N/A
        menu set Message Averaging screen colors ...
        menu getnum Radius
        set %radius #menures
        menu set Radius %radius
        gosub GetAverageScreenColor %x %y %radius
        set %color #result
        menu set CurrentRed %r
        menu set CurrentGreen %g
        menu set CurrentBlue %b
        menu set CurrentColor %color
        menu set CurrentIndex ?
        menu Shape CurrentPreview 296 32 17 17 3 7 1 Silver 7 %color
        menu set Message Finding closest match ...
        gosub GetClosestColor %r %g %b
        menu set MatchRed %r
        menu set MatchGreen %g
        menu set MatchBlue %b
        menu set MatchColor %color
        menu set MatchIndex %index
        menu Shape MatchPreview 296 52 17 17 3 7 1 Silver 7 %color
        set %matchindex %index
        menu set Message Color index set, click Set Dye Tub to use matched color.
    return
     
    sub SetDyeTub
        if %matchindex = N/A
        {
            menu set Message Unable to set dye tub, no match found.
            return
        }
        finditem BBG C
        if #findkind = -1
        {
            menu set Message No dyes found.
            return
        }
        set %dyeid #findid
        menu set Message Target the dye tub you wish to set.
        gosub SetLastTarget
        finditem #ltargetid
        if #findkind = -1 || #findtype <> DBG
        {
            menu set Message That is not a dye tub.
            return
        }
        set #lobjectid %dyeid
        set #ltargetid #findid
        set #ltargetkind 1
        event macro 17 0 ; Last Object
        target 3s
        event macro 22 0 ; Last Target
        gosub WaitForHueGump
        gosub IndexToRowColumnBright %matchindex
        click %bright_dec_x %bright_dec_y f
        wait 5
        if %bright > 0
        {
            for %i 1 %bright
            {
                click %bright_inc_x %bright_inc_y f
                wait 5
            }
        }
        set %x %start_x + ( 8 * %column )
        set %y %start_y + ( 8 * %row )
        click %x %y f
        wait 5
        menu set Message Your dye tub has been set.
    return
     
    sub IndexToRowColumnBright
        namespace push
        namespace local IndexToRowColumnBright
        set !c %1 - 2
        set %row !c / 100
        set !c !c - ( %row * 100 )
        set %column !c / 5
        set %bright !c - ( %column * 5 )
        namespace clear
        namespace pop
    return
     
    sub ColorToRGB
        namespace push
        namespace local ColorToRGB
        set !i %1
        set !j !i / 256
        set !k !j * 256
        set %r !i - !k
        set !i !j
        set !j !i / 256
        set !k !j * 256
        set %g !i - !k
        set !i !j
        set !j !i / 256
        set !k !j * 256
        set %b !i - !k
        namespace clear
        namespace pop
    return
     
    sub GetAverageScreenColor
        namespace push
        namespace local GetAverageScreenColor
        set !lpc #lpc
        linespercycle 300
        set !start_x %1 - %3
        set !start_y %2 - %3
        set !end_x %1 + %3
        set !end_y %2 + %3
        set !r 0
        set !g 0
        set !b 0
        set !i 0
        for !y !start_y !end_y
        {
            for !x !start_x !end_x
            {
                savepix !x !y 1
                gosub ColorToRGB #pixcol
                set !r !r + %r
                set !g !g + %g
                set !b !b + %b
                set !i !i + 1
            }
        }
        set %r !r / !i
        set %g !g / !i
        set %b !b / !i
        set #result ( %b * 65536 ) + ( %g * 256 ) + ( %r )
        linespercycle !lpc
        namespace clear
        namespace pop
    return #result
     
    sub GetClosestColor
        namespace push
        namespace local GetClosestColor
        set !lpc #lpc
        linespercycle 300
        set !r %1
        set !g %2
        set !b %3
        set !delta 256
        set %index 0
        set %color 0
        for !i 2 1001
        {
            gosub GetIndexedColor !i
            set !color #result
            gosub ColorToRGB !color
            set !j 0
            set !k %r - !r abs
            if !k > !j
                set !j !k
            set !k %g - !g abs
            if !k > !j
                set !j !k
            set !k %b - !b abs
            if !k > !j
                set !j !k
            if !j < !delta
            {
                set %color !color
                set %index !i
                set !delta !j
                if !delta = 0
                    goto _getclosestcolor_exit
            }
        }
        _getclosestcolor_exit:
        gosub ColorToRGB %color
        linespercycle !lpc
        namespace clear
        namespace pop
    return
     
    sub SetLastTarget
        set #targcurs 1
        _setlasttarget_wait:
        if #targcurs = 1
            goto _setlasttarget_wait
    return
     
    sub WaitForHueGump
        _hue_gump_wait:
        if #contname <> hue_gump
            goto _hue_gump_wait
        set %bright_dec_x #contposx + 35
        set %bright_dec_y #contposy + 149
        set %bright_inc_x #contposx + 189
        set %bright_inc_y #contposy + 149
        set %start_x #contposx + 34
        set %start_y #contposy + 34
    return
     
    sub Reset
        menu set CurrentRed ?
        menu set CurrentGreen ?
        menu set CurrentBlue ?
        menu set CurrentColor ?
        menu set CurrentIndex ?
        menu set MatchRed ?
        menu set MatchGreen ?
        menu set MatchBlue ?
        menu set MatchColor ?
        menu set MatchIndex ?
        menu Shape CurrentPreview 296 32 17 17 3 7 1 Silver 7 Gray
        menu Shape MatchPreview 296 52 17 17 3 7 1 Silver 7 Gray
        set %matchindex N/A
    return
     
    sub showMenu
        menu Clear
        menu Window Title UD Hue Matcher 1.1
        menu Window Color Black
        menu Window Size 330 173
        menu Font Name MS Sans Serif
        menu Font BGColor Black
        menu Font Size 8
        menu Shape Box1 4 4 321 73 3 7 1 Olive 7 Black
        menu Font Color Teal
        menu Font Align Right
        menu Text lblRed 93 12 R
        menu Text lblGreen 123 12 G
        menu Text lblBlue 153 12 B
        menu Text lblColor 214 12 Color
        menu Text lblIndex 258 12 Index
        menu Text lblPreview 312 12 Preview
        menu Font Color Silver
        menu Font Align Left
        menu Text lblCurrent 12 36 Current:
        menu Text lblMatch 12 56 Match:
        menu Font Align Right
        menu Text CurrentRed 93 36 ?
        menu Text CurrentGreen 123 36 ?
        menu Text CurrentBlue 153 36 ?
        menu Text CurrentColor 214 36 ?
        menu Text CurrentIndex 258 36 ?
        menu Shape CurrentPreview 296 32 17 17 3 7 1 Silver 7 Gray
        menu Text MatchRed 93 56 ?
        menu Text MatchGreen 123 56 ?
        menu Text MatchBlue 153 56 ?
        menu Text MatchColor 214 56 ?
        menu Text MatchIndex 258 56 ?
        menu Shape MatchPreview 296 52 17 17 3 7 1 Silver 7 Gray
        menu Shape Box2 4 80 173 69 3 7 1 Olive 7 Black
        menu Font Color Silver
        menu Font Align Left
        menu Text lblRadius 12 93 Radius:
        menu Text lblX 120 88 X:
        menu Text lblY 120 108 Y:
        menu Edit Radius 67 88 33 2
        menu Font Align Right
        menu Text X 168 88 0
        menu Text Y 168 108 0
        menu Font BGColor Gray
        menu Font Color Black
        menu Button btnMatchScreen 12 120 91 25 Match Screen
        menu Shape Box3 180 80 145 69 3 7 1 Olive 7 Black
        menu Button btnSetDyeTub 188 120 131 25 Set Dye Tub
        menu Button btnMatchObject 188 88 131 25 Match Object
        menu Font BGColor Black
        menu Font Color Olive
        menu Font Align Left
        menu Text Message 5 153 Loading color table, please wait ...
        menu Show
        menu HideEUO
    return
     
    sub GetIndexedColor
        namespace push
        namespace local ColorTable
        set #result !color . %1
        namespace pop
    return #result
     
    sub BuildColorTable
        namespace push
        namespace local ColorTable
        menu set Message Open a normal hue gump.
        gosub WaitForHueGump
        menu set Message Building color table, please wait ...
        click %bright_dec_x %bright_dec_y f
        wait 1
        for %bright 0 4
        {
            for %row 0 9
            {
                for %column 0 19
                {
                    set %index 2 + ( 100 * %row ) + ( 5 * %column ) + ( %bright )
                    set %x %start_x + ( %column * 8 )
                    set %y %start_y + ( %row * 8 )
                    wait 1
                    savepix %x %y 1
                    set !color , %index #pixcol
                    if #true ; Set to true for debug
                    {
                        click %x %y f
                        gosub ColorToRGB #pixcol
                        menu set X %x
                        menu set Y %y
                        menu set CurrentRed %r
                        menu set CurrentGreen %g
                        menu set CurrentBlue %b
                        menu set CurrentColor #pixcol
                        menu set CurrentIndex %index
                        menu delete CurrentPreview
                        menu Shape CurrentPreview 296 32 17 17 3 7 1 Silver 7 #pixcol
                        ;wait 2
                    }
                }
            }
            click %bright_inc_x %bright_inc_y f
            wait 1
        }
        namespace pop
    return
     
    sub SaveColorTable
        namespace push
        namespace local ColorTable
        for %i 2 1001
        {
            if #true ; Set to true for debug
            {
               set %color !color . %i
               gosub ColorToRGB %color
               menu set MatchRed %r
               menu set MatchGreen %g
               menu set MatchBlue %b
               menu set MatchColor %color
               menu set MatchIndex %i
               menu delete MatchPreview
               menu Shape MatchPreview 296 52 17 17 3 7 1 Silver 7 %color
            }
            execute cmd /c echo set ! , color , %i , #spc , !color . %i >> ColorTable.euo
        }
        namespace pop
    return
     
    sub LoadColorTable
        set %lpc #lpc
        linespercycle 300
        namespace push
        namespace local ColorTable
        ;call ColorTable.euo
        set !color2 8060928
        set !color3 9699328
        set !color4 9705496
        set !color5 9711921
        set !color6 9718346
        set !color7 8060969
        set !color8 9699377
        set !color9 9705538
        set !color10 9711954
        set !color11 9718371
        set !color12 8060994
        set !color13 9699410
        set !color14 9705571
        set !color15 9711979
        set !color16 9718387
        set !color17 8061035
        set !color18 9699460
        set !color19 9705604
        set !color20 9712012
        set !color21 9718412
        set !color22 5898363
        set !color23 7536788
        set !color24 7542932
        set !color25 8073620
        set !color26 8669844
        set !color27 3735675
        set !color28 4849812
        set !color29 5904532
        set !color30 6500756
        set !color31 7555732
        set !color32 1572987
        set !color33 2162836
        set !color34 3217556
        set !color35 4862356
        set !color36 5917332
        set !color37 2171
        set !color38 4244
        set !color39 1583508
        set !color40 3226004
        set !color41 4870804
        set !color42 12667
        set !color43 14740
        set !color44 1591956
        set !color45 3234452
        set !color46 4877204
        set !color47 21115
        set !color48 25492
        set !color49 1600404
        set !color50 3240852
        set !color51 4881300
        set !color52 31611
        set !color53 38036
        set !color54 1610900
        set !color55 3249300
        set !color56 4887700
        set !color57 31570
        set !color58 37987
        set !color59 1610859
        set !color60 3249267
        set !color61 4887675
        set !color62 31537
        set !color63 37945
        set !color64 1610826
        set !color65 3249242
        set !color66 4887659
        set !color67 31496
        set !color68 37904
        set !color69 1610793
        set !color70 3249209
        set !color71 4887634
        set !color72 1604352
        set !color73 2200576
        set !color74 3249176
        set !color75 4887601
        set !color76 5936202
        set !color77 3767040
        set !color78 4887552
        set !color79 5936152
        set !color80 6526001
        set !color81 7574602
        set !color82 5929728
        set !color83 7574528
        set !color84 7574552
        set !color85 8098865
        set !color86 8688714
        set !color87 8088320
        set !color88 9733120
        set !color89 9733144
        set !color90 9735217
        set !color91 9735242
        set !color92 8077824
        set !color93 9720320
        set !color94 9724696
        set !color95 9726769
        set !color96 9728842
        set !color97 8071424
        set !color98 9711872
        set !color99 9716248
        set !color100 9720369
        set !color101 9724746
        set !color102 7538696
        set !color103 8652808
        set !color104 9183521
        set !color105 9189689
        set !color106 9196114
        set !color107 7538729
        set !color108 8652849
        set !color109 9183554
        set !color110 9189714
        set !color111 9196131
        set !color112 7538754
        set !color113 8652882
        set !color114 9183587
        set !color115 9189739
        set !color116 9196147
        set !color117 7538787
        set !color118 8652923
        set !color119 9183620
        set !color120 9189764
        set !color121 9196172
        set !color122 5900403
        set !color123 7014532
        set !color124 7545228
        set !color125 8075660
        set !color126 8671884
        set !color127 3737715
        set !color128 4851844
        set !color129 5906828
        set !color130 6502796
        set !color131 7557772
        set !color132 2164851
        set !color133 2164868
        set !color134 3744140
        set !color135 4864396
        set !color136 5919372
        set !color137 528499
        set !color138 530564
        set !color139 2175372
        set !color140 3752588
        set !color141 5397132
        set !color142 536947
        set !color143 539012
        set !color144 2181772
        set !color145 3758732
        set !color146 5401484
        set !color147 545395
        set !color148 549764
        set !color149 2190220
        set !color150 3765132
        set !color151 5405580
        set !color152 553843
        set !color153 558212
        set !color154 2198668
        set !color155 3771532
        set !color156 5409932
        set !color157 553810
        set !color158 558179
        set !color159 2198635
        set !color160 3771507
        set !color161 5409915
        set !color162 553777
        set !color163 558137
        set !color164 2198602
        set !color165 3771482
        set !color166 5409899
        set !color167 553744
        set !color168 558104
        set !color169 2198577
        set !color170 3771458
        set !color171 5409882
        set !color172 2192136
        set !color173 2196488
        set !color174 3771425
        set !color175 4885561
        set !color176 5934162
        set !color177 3765000
        set !color178 4883464
        set !color179 5934113
        set !color180 6523961
        set !color181 7572562
        set !color182 5927688
        set !color183 7046152
        set !color184 7572513
        set !color185 8096825
        set !color186 8686674
        set !color187 7561992
        set !color188 8682248
        set !color189 9208865
        set !color190 9208889
        set !color191 9210962
        set !color192 7553544
        set !color193 8671752
        set !color194 9200417
        set !color195 9202489
        set !color196 9204562
        set !color197 7547144
        set !color198 8663304
        set !color199 9191969
        set !color200 9196089
        set !color201 9200466
        set !color202 7014408
        set !color203 8654864
        set !color204 8661289
        set !color205 9189689
        set !color206 9196114
        set !color207 7014441
        set !color208 8654897
        set !color209 8661314
        set !color210 9189714
        set !color211 9196131
        set !color212 7014466
        set !color213 8654930
        set !color214 8661338
        set !color215 9189739
        set !color216 9196147
        set !color217 7014490
        set !color218 8654963
        set !color219 8661371
        set !color220 9189755
        set !color221 9196164
        set !color222 5376107
        set !color223 7016580
        set !color224 7547268
        set !color225 8075660
        set !color226 8671884
        set !color227 3737707
        set !color228 4853892
        set !color229 5908868
        set !color230 6502796
        set !color231 7557772
        set !color232 2164843
        set !color233 2691204
        set !color234 3746180
        set !color235 4864396
        set !color236 6509196
        set !color237 530539
        set !color238 1057156
        set !color239 2699652
        set !color240 3754636
        set !color241 5397132
        set !color242 536939
        set !color243 1063300
        set !color244 2706052
        set !color245 3758732
        set !color246 5401484
        set !color247 545387
        set !color248 1074052
        set !color249 2714500
        set !color250 3765132
        set !color251 5405580
        set !color252 551787
        set !color253 1082500
        set !color254 2720900
        set !color255 3771532
        set !color256 5409932
        set !color257 551762
        set !color258 1082467
        set !color259 2720875
        set !color260 3771507
        set !color261 5409915
        set !color262 551729
        set !color263 1082425
        set !color264 2720842
        set !color265 3771482
        set !color266 5409899
        set !color267 551704
        set !color268 1082401
        set !color269 2720817
        set !color270 3771466
        set !color271 5409882
        set !color272 2190088
        set !color273 2720784
        set !color274 3769385
        set !color275 4885561
        set !color276 6523986
        set !color277 3762952
        set !color278 4883472
        set !color279 5932073
        set !color280 6523961
        set !color281 7572562
        set !color282 5401352
        set !color283 7046160
        set !color284 7570473
        set !color285 8096825
        set !color286 8686674
        set !color287 7035400
        set !color288 8680208
        set !color289 8682281
        set !color290 9206585
        set !color291 9208914
        set !color292 7029256
        set !color293 8671760
        set !color294 8673833
        set !color295 9202489
        set !color296 9204562
        set !color297 7022856
        set !color298 8663312
        set !color299 8667689
        set !color300 9196089
        set !color301 9200466
        set !color302 6494232
        set !color303 8067096
        set !color304 8073521
        set !color305 8667714
        set !color306 9198170
        set !color307 6494249
        set !color308 8067129
        set !color309 8073546
        set !color310 8667738
        set !color311 9198187
        set !color312 6494274
        set !color313 8067154
        set !color314 8073562
        set !color315 8667755
        set !color316 9198195
        set !color317 6494298
        set !color318 8067179
        set !color319 8073587
        set !color320 8667771
        set !color321 9198212
        set !color322 5380195
        set !color323 6494331
        set !color324 7025019
        set !color325 7553668
        set !color326 8084108
        set !color327 3741795
        set !color328 4855931
        set !color329 5910907
        set !color330 6505092
        set !color331 7559820
        set !color332 2693219
        set !color333 3217531
        set !color334 4338043
        set !color335 5390980
        set !color336 6511244
        set !color337 1579107
        set !color338 1581435
        set !color339 3225979
        set !color340 4344452
        set !color341 5921420
        set !color342 1585507
        set !color343 1589883
        set !color344 3230331
        set !color345 4348548
        set !color346 5925772
        set !color347 1591907
        set !color348 1596027
        set !color349 3236731
        set !color350 4352900
        set !color351 5929868
        set !color352 1598307
        set !color353 1604475
        set !color354 3242875
        set !color355 4359300
        set !color356 5934220
        set !color357 1598282
        set !color358 1604442
        set !color359 3242851
        set !color360 4359275
        set !color361 5934203
        set !color362 1598257
        set !color363 1604418
        set !color364 3242826
        set !color365 4359258
        set !color366 5934187
        set !color367 1598232
        set !color368 1604385
        set !color369 3242809
        set !color370 4359242
        set !color371 5934170
        set !color372 2712344
        set !color373 3242776
        set !color374 4356913
        set !color375 5407810
        set !color376 6523994
        set !color377 3760920
        set !color378 4881176
        set !color379 5929777
        set !color380 6521922
        set !color381 7572570
        set !color382 5399320
        set !color383 6519576
        set !color384 7043889
        set !color385 7570498
        set !color386 8096858
        set !color387 6511128
        set !color388 8088344
        set !color389 8090417
        set !color390 8682306
        set !color391 9208922
        set !color392 6504984
        set !color393 8081944
        set !color394 8084017
        set !color395 8678210
        set !color396 9204570
        set !color397 6498584
        set !color398 8075544
        set !color399 8079921
        set !color400 8673858
        set !color401 9202522
        set !color402 5904408
        set !color403 7545121
        set !color404 7549233
        set !color405 8079946
        set !color406 8673882
        set !color407 5904433
        set !color408 7545145
        set !color409 7549258
        set !color410 8079962
        set !color411 8673899
        set !color412 5904450
        set !color413 7545170
        set !color414 7549274
        set !color415 8079979
        set !color416 8673907
        set !color417 5904466
        set !color418 7545195
        set !color419 7549299
        set !color420 8079995
        set !color421 8673924
        set !color422 5380186
        set !color423 6496627
        set !color424 7025011
        set !color425 7555707
        set !color426 8084100
        set !color427 3741786
        set !color428 4858227
        set !color429 5910899
        set !color430 6507131
        set !color431 7559812
        set !color432 2693210
        set !color433 3219827
        set !color434 4338035
        set !color435 5393019
        set !color436 6511236
        set !color437 1581402
        set !color438 2173299
        set !color439 3225971
        set !color440 4868731
        set !color441 5923716
        set !color442 1585498
        set !color443 2179699
        set !color444 3232371
        set !color445 4872827
        set !color446 5925764
        set !color447 1591898
        set !color448 2183795
        set !color449 3236723
        set !color450 4877179
        set !color451 5927812
        set !color452 1595994
        set !color453 2192235
        set !color454 3240819
        set !color455 4881275
        set !color456 5932164
        set !color457 1595978
        set !color458 2192210
        set !color459 3240803
        set !color460 4881259
        set !color461 5932147
        set !color462 1595953
        set !color463 2192194
        set !color464 3240786
        set !color465 4881242
        set !color466 5932139
        set !color467 1595937
        set !color468 2192169
        set !color469 3240761
        set !color470 4881226
        set !color471 5932131
        set !color472 2710040
        set !color473 3240737
        set !color474 4354865
        set !color475 5405514
        set !color476 6521946
        set !color477 3758616
        set !color478 4879137
        set !color479 5927729
        set !color480 6519626
        set !color481 7570522
        set !color482 5397016
        set !color483 6517537
        set !color484 7041841
        set !color485 7568202
        set !color486 8094810
        set !color487 5919256
        set !color488 7564065
        set !color489 7566129
        set !color490 8092490
        set !color491 8684634
        set !color492 5915160
        set !color493 7557665
        set !color494 7559729
        set !color495 8088394
        set !color496 8680282
        set !color497 5910808
        set !color498 7551265
        set !color499 7555633
        set !color500 8084042
        set !color501 8678234
        set !color502 5382433
        set !color503 7022889
        set !color504 7551289
        set !color505 8079946
        set !color506 8676195
        set !color507 5382449
        set !color508 7022905
        set !color509 7551306
        set !color510 8079962
        set !color511 8676203
        set !color512 5382466
        set !color513 7022930
        set !color514 7551322
        set !color515 8079979
        set !color516 8676211
        set !color517 5382482
        set !color518 7022947
        set !color519 7551339
        set !color520 8079987
        set !color521 8676219
        set !color522 4858194
        set !color523 5908843
        set !color524 6502771
        set !color525 7031419
        set !color526 8086404
        set !color527 3744082
        set !color528 4860267
        set !color529 5912947
        set !color530 6507131
        set !color531 7562116
        set !color532 2695506
        set !color533 3746155
        set !color534 4864371
        set !color535 5917307
        set !color536 7037828
        set !color537 2173266
        set !color538 2699627
        set !color539 3752563
        set !color540 4870779
        set !color541 6513540
        set !color542 2175314
        set !color543 2703979
        set !color544 3756659
        set !color545 4872827
        set !color546 6515588
        set !color547 2179666
        set !color548 2708075
        set !color549 3761011
        set !color550 4877179
        set !color551 6517636
        set !color552 2183762
        set !color553 2714475
        set !color554 3765107
        set !color555 4881275
        set !color556 6521988
        set !color557 2183746
        set !color558 2714450
        set !color559 3765091
        set !color560 4881259
        set !color561 6521971
        set !color562 2183729
        set !color563 2714434
        set !color564 3765074
        set !color565 4881242
        set !color566 6521963
        set !color567 2183721
        set !color568 2714417
        set !color569 3765058
        set !color570 4881234
        set !color571 6521955
        set !color572 2708001
        set !color573 3762985
        set !color574 4879161
        set !color575 5929802
        set !color576 7046243
        set !color577 3756577
        set !color578 4877097
        set !color579 5927737
        set !color580 6519626
        set !color581 7570531
        set !color582 4870689
        set !color583 5925673
        set !color584 6517561
        set !color585 7043914
        set !color586 8094819
        set !color587 5394977
        set !color588 7037737
        set !color589 7564089
        set !color590 8090442
        set !color591 8682339
        set !color592 5390881
        set !color593 7033385
        set !color594 7559737
        set !color595 8088394
        set !color596 8680291
        set !color597 5386529
        set !color598 7026985
        set !color599 7555641
        set !color600 8084042
        set !color601 8678243
        set !color602 5384489
        set !color603 6500657
        set !color604 7029314
        set !color605 7557714
        set !color606 8086371
        set !color607 5384497
        set !color608 6500674
        set !color609 7029322
        set !color610 7557722
        set !color611 8086379
        set !color612 5384514
        set !color613 6500682
        set !color614 7029338
        set !color615 7557731
        set !color616 8086387
        set !color617 5384522
        set !color618 6500698
        set !color619 7029347
        set !color620 7557739
        set !color621 8086395
        set !color622 4335954
        set !color623 5386595
        set !color624 6505067
        set !color625 7033459
        set !color626 7562107
        set !color627 3746130
        set !color628 4862307
        set !color629 5915243
        set !color630 6509171
        set !color631 7562107
        set !color632 3221842
        set !color633 3748195
        set !color634 4866667
        set !color635 5919347
        set !color636 7037819
        set !color637 2697554
        set !color638 3225955
        set !color639 4344427
        set !color640 5397107
        set !color641 6515579
        set !color642 2701650
        set !color643 3230307
        set !color644 4346475
        set !color645 5399411
        set !color646 6517627
        set !color647 2703954
        set !color648 3232355
        set !color649 4348523
        set !color650 5401459
        set !color651 6517627
        set !color652 2708050
        set !color653 3236707
        set !color654 4352875
        set !color655 5403507
        set !color656 6519675
        set !color657 2708034
        set !color658 3236690
        set !color659 4352858
        set !color660 5403499
        set !color661 6519667
        set !color662 2708025
        set !color663 3236682
        set !color664 4352850
        set !color665 5403491
        set !color666 6519667
        set !color667 2708009
        set !color668 3236665
        set !color669 4352842
        set !color670 5403482
        set !color671 6519659
        set !color672 3232297
        set !color673 3760945
        set !color674 4877122
        set !color675 5927762
        set !color676 7043939
        set !color677 3756585
        set !color678 4875057
        set !color679 5925698
        set !color680 6517586
        set !color681 7568227
        set !color682 4346409
        set !color683 5399345
        set !color684 6515522
        set !color685 7041874
        set !color686 7568227
        set !color687 5392937
        set !color688 6511153
        set !color689 7037762
        set !color690 7564114
        set !color691 8092515
        set !color692 5390889
        set !color693 6507057
        set !color694 7035458
        set !color695 7562066
        set !color696 8090467
        set !color697 5386537
        set !color698 6505009
        set !color699 7031362
        set !color700 7559762
        set !color701 8088419
        set !color702 4862257
        set !color703 5912889
        set !color704 6507082
        set !color705 7035482
        set !color706 7564139
        set !color707 4862257
        set !color708 5912898
        set !color709 6507090
        set !color710 7035482
        set !color711 7564139
        set !color712 4862274
        set !color713 5912906
        set !color714 6507098
        set !color715 7035491
        set !color716 7564147
        set !color717 4862274
        set !color718 5912914
        set !color719 6507107
        set !color720 7035499
        set !color721 7564147
        set !color722 4337994
        set !color723 5388634
        set !color724 5917283
        set !color725 7035499
        set !color726 7564147
        set !color727 3748170
        set !color728 4864346
        set !color729 5917283
        set !color730 6511211
        set !color731 7564147
        set !color732 3223882
        set !color733 4340058
        set !color734 4868707
        set !color735 5921387
        set !color736 7039859
        set !color737 3223882
        set !color738 3750234
        set !color739 4868707
        set !color740 5921387
        set !color741 7039859
        set !color742 3225930
        set !color743 3754586
        set !color744 4870755
        set !color745 5923691
        set !color746 7041907
        set !color747 3228234
        set !color748 3756634
        set !color749 4872803
        set !color750 5925739
        set !color751 7041907
        set !color752 3230282
        set !color753 3758682
        set !color754 4875107
        set !color755 5925739
        set !color756 7041907
        set !color757 3230274
        set !color758 3758674
        set !color759 4875098
        set !color760 5925739
        set !color761 7041907
        set !color762 3230265
        set !color763 3758666
        set !color764 4875090
        set !color765 5925731
        set !color766 7041907
        set !color767 3230257
        set !color768 3758649
        set !color769 4875082
        set !color770 5925722
        set !color771 7041899
        set !color772 3230257
        set !color773 4348473
        set !color774 4875082
        set !color775 5925722
        set !color776 7041899
        set !color777 3754545
        set !color778 4872761
        set !color779 5923658
        set !color780 6515546
        set !color781 7566187
        set !color782 4344369
        set !color783 5397049
        set !color784 5923658
        set !color785 7039834
        set !color786 7566187
        set !color787 4866609
        set !color788 5919289
        set !color789 6513482
        set !color790 7039834
        set !color791 7566187
        set !color792 4866609
        set !color793 5917241
        set !color794 6511178
        set !color795 7037786
        set !color796 7566187
        set !color797 4862257
        set !color798 5915193
        set !color799 6509130
        set !color800 7035482
        set !color801 7564139
        set !color802 4337969
        set !color803 5390914
        set !color804 5917258
        set !color805 7035482
        set !color806 7564139
        set !color807 4337977
        set !color808 5390922
        set !color809 5917266
        set !color810 7035491
        set !color811 7564147
        set !color812 4337986
        set !color813 5390922
        set !color814 5917274
        set !color815 7035491
        set !color816 7564147
        set !color817 4337986
        set !color818 5390930
        set !color819 5917274
        set !color820 7035499
        set !color821 7564147
        set !color822 4337986
        set !color823 4866642
        set !color824 5917274
        set !color825 6511211
        set !color826 7564147
        set !color827 3748162
        set !color828 4866642
        set !color829 5917274
        set !color830 6511211
        set !color831 7564147
        set !color832 3748162
        set !color833 4866642
        set !color834 5392986
        set !color835 6511211
        set !color836 7564147
        set !color837 3223874
        set !color838 4342354
        set !color839 4870746
        set !color840 5921387
        set !color841 7039859
        set !color842 3225922
        set !color843 4344402
        set !color844 4870746
        set !color845 5923691
        set !color846 7041907
        set !color847 3228226
        set !color848 4344402
        set !color849 4872794
        set !color850 5923691
        set !color851 7041907
        set !color852 3228226
        set !color853 4346450
        set !color854 4872794
        set !color855 5925739
        set !color856 7041907
        set !color857 3228226
        set !color858 4346442
        set !color859 4872794
        set !color860 5925731
        set !color861 7041907
        set !color862 3228217
        set !color863 4346442
        set !color864 4872786
        set !color865 5925731
        set !color866 7041907
        set !color867 3228209
        set !color868 4346434
        set !color869 4872786
        set !color870 5925722
        set !color871 7041899
        set !color872 3752497
        set !color873 4870722
        set !color874 5397066
        set !color875 6515546
        set !color876 7566187
        set !color877 3752497
        set !color878 4870722
        set !color879 5921354
        set !color880 6515546
        set !color881 7566187
        set !color882 4342321
        set !color883 4870722
        set !color884 5921354
        set !color885 6515546
        set !color886 7566187
        set !color887 4342321
        set !color888 5395010
        set !color889 5921354
        set !color890 7039834
        set !color891 7566187
        set !color892 4342321
        set !color893 5392962
        set !color894 5921354
        set !color895 7037786
        set !color896 7566187
        set !color897 4340017
        set !color898 5392962
        set !color899 5919306
        set !color900 7037786
        set !color901 7566187
        set !color902 3750201
        set !color903 4868682
        set !color904 5921370
        set !color905 6513507
        set !color906 7566195
        set !color907 3750201
        set !color908 4868682
        set !color909 5921370
        set !color910 6513507
        set !color911 7566195
        set !color912 3750201
        set !color913 4868682
        set !color914 5921370
        set !color915 6513507
        set !color916 7566195
        set !color917 3750201
        set !color918 4868682
        set !color919 5921370
        set !color920 6513507
        set !color921 7566195
        set !color922 3750201
        set !color923 4868682
        set !color924 5921370
        set !color925 6513507
        set !color926 7566195
        set !color927 3750201
        set !color928 4868682
        set !color929 5921370
        set !color930 6513507
        set !color931 7566195
        set !color932 3750201
        set !color933 4868682
        set !color934 5921370
        set !color935 6513507
        set !color936 7566195
        set !color937 3750201
        set !color938 4868682
        set !color939 5921370
        set !color940 6513507
        set !color941 7566195
        set !color942 3750201
        set !color943 4868682
        set !color944 5921370
        set !color945 6513507
        set !color946 7566195
        set !color947 3750201
        set !color948 4868682
        set !color949 5921370
        set !color950 6513507
        set !color951 7566195
        set !color952 3750201
        set !color953 4868682
        set !color954 5921370
        set !color955 6513507
        set !color956 7566195
        set !color957 3750201
        set !color958 4868682
        set !color959 5921370
        set !color960 6513507
        set !color961 7566195
        set !color962 3750201
        set !color963 4868682
        set !color964 5921370
        set !color965 6513507
        set !color966 7566195
        set !color967 3750201
        set !color968 4868682
        set !color969 5921370
        set !color970 6513507
        set !color971 7566195
        set !color972 3750201
        set !color973 4868682
        set !color974 5921370
        set !color975 6513507
        set !color976 7566195
        set !color977 3750201
        set !color978 4868682
        set !color979 5921370
        set !color980 6513507
        set !color981 7566195
        set !color982 3750201
        set !color983 4868682
        set !color984 5921370
        set !color985 6513507
        set !color986 7566195
        set !color987 3750201
        set !color988 4868682
        set !color989 5921370
        set !color990 6513507
        set !color991 7566195
        set !color992 3750201
        set !color993 4868682
        set !color994 5921370
        set !color995 6513507
        set !color996 7566195
        set !color997 3750201
        set !color998 4868682
        set !color999 5921370
        set !color1000 6513507
        set !color1001 7566195
        namespace pop
        linespercycle %lpc
    return
