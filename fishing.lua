;; Fishing script by Pukas
;; 20070127

;; FISHING POLE ID TO SET!

set %v_pole_obj_id LEYBYZD

;; OTHER VARIABLES
set %v_fishing_radius 8
set %v_reset_time 20


;; DON'T MODIFY BELOW THIS COMMENT UNLESS YOU KNOW WHAT YOU ARE DOING

;; GLOBAL VARS

set %g_rel_pos_x 0
set %g_rel_pos_y 0

set %g_finished_msgs_var g_finished_msg
set %g_finished_msgs_cnt 0

set %g_last_charposx #charposx
set %g_last_charposy #charposy

set %g_max_x %v_fishing_radius
set %g_min_x %v_fishing_radius * -1
set %g_max_y %v_fishing_radius
set %g_min_y %v_fishing_radius * -1

;; STATIC VALUES

set %s_finished_msgs you_stop_fishing|you_can't_use_that_on_this|you_can't_seem_to_get_any_fish_here
set %s_blocking_tiles wooden_plank_pier_stone_sand
set %s_default_lpc 100

;; DEBUG FLAG
set %DEBUG #false

;; SCRIPT

tile init
initEvents

event sysmessage Fishing done fast by Pukas
event sysmessage Area: %v_fishing_radius tiles around char

gosub string exp %s_finished_msgs | %g_finished_msgs_var
set %g_finished_msgs_cnt #result

wait 2s

event sysmessage Scanning for water...
gosub scanArea
if ! #result
{
   event sysmessage Aborting script...
   halt
}

mainLoop:
event sysmessage Starting new fishing loop

for %g_rel_pos_x  %g_min_x %g_max_x
{
     for %g_rel_pos_y  %g_min_y %g_max_y
     {
         if #charposx <> %g_last_charposx || #charposy <> %g_last_charposy
         {
            event sysmessage You moved, rescanning area!
            gosub scanArea

            if ! #result
            {
               event sysmessage Aborting script...
               halt
            }

            set %g_last_charposx #charposx
            set %g_last_charposy #charposy
         }
         set !pos_x #charposx + %g_rel_pos_x
         set !pos_y #charposy + %g_rel_pos_y

         gosub checkIfWater !pos_x !pos_y

         if ! #result
         {
            continue
         }

         event sysmessage Fishing at ( , !pos_x , , , !pos_y , )

         set #lobjectid %v_pole_obj_id

         event macro 17
         target 5s
         event macro 22

         wait 2s

         for !t 1 %v_reset_time
         {
             gosub checkIfDone

             wait 1s

             if #result
             {
                break
             }
         }
     }
}

goto mainLoop


;; Check how many tiles of water there are
sub scanArea

set !count 0
set #lpc 1000

for !x %g_min_x %g_max_x
{
     for !y %g_min_y %g_max_y
     {
         set !pos_x #charposx + !x
         set !pos_y #charposy + !y

         gosub checkIfWater !pos_x !pos_y

         if #result
         {
            set !count !count + 1
         }
     }
}

set #lpc %s_default_lpc

if !count = 0
{
   event sysmessage No positions available for fishing!
   return #false
}

event sysmessage Found !count tiles to fish from

return #true


;; Check if fishin loop is done
sub checkIfDone

if %g_finished_msgs_cnt = 0
{
   display No finished msgs found! Aborting!
   halt
}

for !i 1 %g_finished_msgs_cnt
{
    if %g_finished_msg . !i in #sysmsg
    {
       if %DEBUG
       {
          event sysmessage Stopped fishing! Found: %g_finished_msg . !i
       }
       return #true
    }

}
return #false



;; checkIfWater XPOS YPOS
sub checkIfWater

if %0 <> 2
{
   return #false
}

set !waterfound #false

tile cnt %1 %2 #curskind

for %tilekind 0 #tilecnt
{
   tile get %1 %2 %tilekind #curskind
   if water in #tilename
   {
      set #ltargetx %1
      set #ltargety %2
      set #ltargetz #tilez
      set #ltargettile #tiletype
      set #ltargetkind 3
      set !waterfound #true
   }
   if #tilename in %s_blocking_tiles
   {
       return #false
   }
}

if !waterfound
{
   return #true
}

return #false

;==================================
; Script Name: Gravisoft's String Implode/Explode SUB
; Author: Gravix
; Version: 1.1
; Client Tested with: 4.0.6a
; EUO version tested with: 1.42.009F
; Shard OSI / FS: Both
; Revision Date: 1/19/04
; Public Release: 1/01/04
; Global Variables Used: None
; Purpose: Implode: Take an array and link into a string separated by a value/char
; Explode: Take a string and break into array based on dividers
;==================================

;SUB string exp %str %div %arry
SUB String
if %5 <> #true
    {
    namespace push
    namespace local Implode-Explode
    }
IF %1 = exp
     {
      SET !c 0
      SET !s %2
    break:
        IF %3 in !s
            {
             SET !c !c + 1
              STR POS !s %3
              SET !p0 #STRRES
              SET !p1 !p0 - 1
              STR LEFT !s !p1
              if %5 <> #true
            SET % . %4 , !c #STRRES
        if %5
            SET ! . %4 , !c #STRRES
              STR DEL !s 1 !p0
              SET !s #STRRES
              GOTO break
            }
        ELSE
            {
            SET !c !c + 1
        if %5
                  SET ! . %4 , !c !s
              Set #result !c
        if %5 <> #true
                  {
                  SET % . %4 , !c !s
            Namespace clear
          Namespace pop
            }
        RETURN #result
            }
      }
RETURN #FALSE
